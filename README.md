# Máquina de Lavar Calçados

Projeto desenvolvido na disciplina de Projeto Integrador 2, na Universidade de Brasília - Campus Gama.

## O projeto

Uma máquina de lavar calçados integrada com um software para gerenciamento de lavanderias.

## Repositórios de código

* [Web Application](https://gitlab.com/wash-machine/web)
* [API Server](https://gitlab.com/wash-machine/api)
    * [API Deploy - Para uso de desenvolvedores](https://shoe-washer.herokuapp.com/api/rest-auth/login/)   
* [Mobile Application](https://gitlab.com/wash-machine/app)
* [Controller - Raspberry](https://gitlab.com/wash-machine/Communication)
* [Documentação Relatórios](https://gitlab.com/wash-machine/documentation)

## Integrantes

* Amanda Martins, 14/0015426
* Artur Bersan, 14/0016813
* Danovan Martins, 13/0044008
* Iasmin Mendes, 14/0041940
* Júlia Henriques, 14/0056432
* Juliana Motta, 14/0147233
* Júlio César Xavier, 14/0024140
* Karine Ximenes, 11/0033388
* Larissa Vidal, 12/0124459
* Marcelo Herton, 14/0056688
* Renato Lucas, 13/0132055
* Tiago Martins, 13/0048879
* Valéria Amâncio, 13/0038474


